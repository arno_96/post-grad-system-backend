const admin = require("firebase-admin");
const serviceAccount = require("../firebase_credentials.json");
const uuidv4 = require("uuid/v4");

class FireStorage {
  constructor() {
    admin.initializeApp({
      credential: admin.credential.cert(serviceAccount),
      databaseURL: "https://e-and-e-postgraduate-system.firebaseio.com"
    });

    this.db = admin.firestore();

    this.db.settings({ timestampsInSnapshots: true });
    console.log("firestore init");
  }

  getStudents() {
    return new Promise((resolve, reject) => {
      var students = [];
      var studentsRef = this.db.collection("students");
      studentsRef
        .get()
        .then(snapshot => {
          snapshot.forEach(doc => {
            let student = doc.data();
            student.id = doc.id;
            students.push(student);
          });
          return resolve(students);
        })
        .catch(err => {
          console.log("Error getting documents", err);
          return reject(err);
        });
    });
  }

  postApplication(application) {
    return new Promise((resolve, reject) => {
      let id = uuidv4();
      var addDoc = this.db.collection("applications").doc(id);
      application.status = "PENDING";
      return addDoc.set(application).then(() => {
        application.id = id;
        return resolve(application);
      });
    });
  }

  postExaminationReport(examinationId, report, examinerId) {
    return new Promise((resolve, reject) => {
      var examinationRef = this.db
        .collection("examinations")
        .doc(examinationId);
      return examinationRef.get().then(doc => {
        let examiners = doc.data().examiners;
        examiners.forEach(item => {
          if (examinerId === item.id) {
            item.report = report;
          }
        });
        examinationRef.update({ examiners: examiners });
        return resolve();
      });
    });
  }

  createExamination(examination) {
    return new Promise((resolve, reject) => {
      let id = uuidv4();
      var addDoc = this.db.collection("examinations").doc(id);
      examination.status = "PENDING";
      examination.id = id;
      examination.examiners.forEach(item => {
        item.verified = false;
        item.id = uuidv4();
      });
      return addDoc.set(examination).then(() => {
        examination.id = id;
        return resolve(examination);
      });
    });
  }

  createStudent(student) {
    return new Promise((resolve, reject) => {
      var addDoc = this.db.collection("students").doc();
      return addDoc.set(student).then(() => {
        return resolve(student);
      });
    });
  }

  verifyEmail(email) {
    return new Promise((resolve, reject) => {
      let supervisorsRef = this.db.collection("supervisors");
      var query = supervisorsRef
        .where("email", "==", email)
        .get()
        .then(snapshot => {
          if (!snapshot.empty) {
            return resolve(true);
          } else {
            return resolve(false);
          }
        })
        .catch(err => {
          return reject(err);
        });
    });
  }

  setApplicationStatus(applicationId, status) {
    return new Promise((resolve, reject) => {
      var applicationRef = this.db
        .collection("applications")
        .doc(applicationId);
      applicationRef
        .update({ status: status })
        .then(() => {
          return resolve();
        })
        .catch(err => {
          //console.log("Error getting documents", err);
          return reject(err);
        });
    });
  }

  getApplications() {
    return new Promise((resolve, reject) => {
      var supervisors = [];
      var supervisorsRef = this.db.collection("applications");
      supervisorsRef
        .get()
        .then(snapshot => {
          snapshot.forEach(doc => {
            let data = doc.data();
            data.id = doc.id;
            supervisors.push(data);
          });
          return resolve(supervisors);
        })
        .catch(err => {
          console.log("Error getting documents", err);
          return reject(err);
        });
    });
  }

  getApplication(applicationId) {
    return new Promise((resolve, reject) => {
      var supervisorsRef = this.db
        .collection("applications")
        .doc(applicationId);
      supervisorsRef
        .get()
        .then(doc => {
          return resolve(doc.data());
        })
        .catch(err => {
          console.log("Error getting documents", err);
          return reject(err);
        });
    });
  }

  getStudent(id) {
    return new Promise((resolve, reject) => {
      var studentsRef = this.db.collection("students").doc(id);
      return studentsRef
        .get()
        .then(doc => {
          return resolve(doc.data());
        })
        .catch(err => {
          console.log("Error getting documents", err);
          return reject(err);
        });
    });
  }

  getExaminationByStudent(id) {
    return new Promise((resolve, reject) => {
      let examinations = [];
      var studentsRef = this.db.collection("examinations");
      let query = studentsRef.where("student.id", "==", id);
      return query
        .get()
        .then(snapshot => {
          snapshot.forEach(doc => {
            examinations.push(doc.data());
            examinations[0].submissions.reverse();
          });
          return resolve(examinations);
        })
        .catch(err => {
          console.log("Error getting documents", err);
          return reject(err);
        });
    });
  }

  getExaminationPortal(examinationId, examinerId) {
    return new Promise((resolve, reject) => {
      var examinationsRef = this.db
        .collection("examinations")
        .doc(examinationId);
      return examinationsRef
        .get()
        .then(doc => {
          let exam = doc.data();
          exam.examiners.forEach(item => {
            if (item.id === examinerId) {
              let data = {
                examiner: item,
                student: exam.student,
                submissions: exam.submissions
              };
              return resolve(data);
            }
          });
          return;
        })
        .catch(err => {
          console.log("Error getting documents", err);
          return reject(err);
        });
    });
  }

  getExaminationById(id) {
    return new Promise((resolve, reject) => {
      var examinationsRef = this.db.collection("examinations").doc(id);
      return examinationsRef
        .get()
        .then(doc => {
          return resolve(doc.data());
        })
        .catch(err => {
          console.log("Error getting documents", err);
          return reject(err);
        });
    });
  }

  verifyExaminer(examinationId, examinerId) {
    return new Promise((resolve, reject) => {
      var examinationsRef = this.db
        .collection("examinations")
        .doc(examinationId);
      return examinationsRef
        .get()
        .then(doc => {
          let examiners = doc.data().examiners;
          examiners.forEach(item => {
            if (item.id === examinerId) {
              if (!item.verified) {
                item.verified = true;
              }
            }
          });
          doc.ref.update({ examiners: examiners });
          return resolve();
        })
        .catch(err => {
          console.log("Error getting documents", err);
          return reject(err);
        });
    });
  }

  uploadSubmission(id, submission) {
    return new Promise((resolve, reject) => {
      let submissions = [];
      var examinationsRef = this.db.collection("examinations");
      let query = examinationsRef.where("student.id", "==", id);
      return query
        .get()
        .then(snapshot => {
          snapshot.forEach(doc => {
            submissions = doc.data().submissions;
            submissions.push(submission);
            doc.ref.update({ submissions: submissions });
          });
          return resolve();
        })
        .catch(err => {
          console.log("Error getting documents", err);
          return reject(err);
        });
    });
  }

  getSupervisors() {
    return new Promise((resolve, reject) => {
      var supervisors = [];
      var supervisorsRef = this.db.collection("supervisors");
      supervisorsRef
        .get()
        .then(snapshot => {
          snapshot.forEach(doc => supervisors.push(doc.data()));
          return resolve(supervisors);
        })
        .catch(err => {
          console.log("Error getting documents", err);
          return reject(err);
        });
    });
  }
}

module.exports = FireStorage;
