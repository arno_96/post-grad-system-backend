const express = require("express");
const router = express.Router();
const auth = require("../tools/auth");
const FireStorage = require("../storage/firestore");
const Mailer = require("../tools/mailer");
let mailer = new Mailer();
let storage = new FireStorage();
let jwt = require("../tools/jwt");
/* GET users listing. */

module.exports = function(passport) {
  router.get("/auth", function(req, res) {
    return auth
      .getAuthUrl()
      .then(result => {
        console.log("API call : getAuthUrl");
        res.json(result);
        return;
      })
      .catch(error => {
        res.status(400);
        res.end();
      });
  });

  router.get(
    "/getStudents",
    passport.authenticate("oauth-bearer", { session: false }),
    function(req, res) {
      console.log("API call : getStudents");
      return storage.getStudents().then(results => {
        res.send(results);
        return;
      });
    }
  );

  router.get(
    "/getStudent",
    passport.authenticate("oauth-bearer", { session: false }),
    function(req, res) {
      let id = req.query.id;
      console.log("API call : getStudent/" + id);
      return storage.getStudent(id).then(results => {
        res.send(results);
        return;
      });
    }
  );

  router.post("/postExaminationReport", function(req, res) {
    console.log("API call : postExaminationReport");
    let report = req.body.report;
    let examinationId = req.body.examinationId;
    let examinerId = req.body.examinerId;
    return storage
      .postExaminationReport(examinationId, report, examinerId)
      .then(result => {
        res.end();
        return;
      });
  });

  router.post("/getToken", function(req, res) {
    console.log("API call : getToken");
    let auth_code = req.body.code;
    return auth
      .getTokenFromCode(auth_code)
      .then(result => {
        const token = result;
        return jwt.decodeToken(result.id_token).then(decodedToken => {
          const username = decodedToken.payload.preferred_username;

          return storage
            .verifyEmail(username)
            .then(result => {
              res.status(200);
              res.json({ token, username });
              return;
            })
            .catch(error => {
              res.status(400);
              res.send(error);
            });
        });
      })
      .catch(error => {
        res.status(400);
        res.end();
      });
  });

  router.post("/postApplication", function(req, res) {
    console.log("API call : postApplication");
    let application = req.body;
    return storage
      .postApplication(application)
      .then(info => {
        return mailer.sendMail(info).then(result => {
          res.end();
          return;
        });
      })
      .catch(error => {
        res.status(400);
        res.end();
      });
  });

  router.post(
    "/uploadSubmission",
    passport.authenticate("oauth-bearer", { session: false }),
    function(req, res) {
      console.log("API call: uploadSubmission");
      let id = req.body.id;
      let submission = req.body.submission;
      return storage
        .uploadSubmission(id, submission)
        .then(() => {
          res.end();
          return;
        })
        .catch(error => {
          res.status(400);
          res.end();
        });
    }
  );

  router.post(
    "/approveApplication",
    passport.authenticate("oauth-bearer", { session: false }),
    function(req, res) {
      console.log("API call : approveApplication");
      let applicationId = req.body.applicationId;
      return storage
        .setApplicationStatus(applicationId, "APPROVED")
        .then(() => {
          res.end();
          return;
        })
        .catch(error => {
          res.status(404);
          res.send("No Application matching request ID");
        });
    }
  );

  router.post(
    "/rejectApplication",
    passport.authenticate("oauth-bearer", { session: false }),
    function(req, res) {
      console.log("API call : rejectApplication");
      let applicationId = req.body.applicationId;
      return storage
        .setApplicationStatus(applicationId, "REJECTED")
        .then(() => {
          res.end();
          return;
        })
        .catch(error => {
          res.status(404);
          res.send("No Application matching request ID");
        });
    }
  );

  router.get("/getSupervisors", function(req, res) {
    console.log("API call : getSupervisors");
    return storage
      .getSupervisors()
      .then(results => {
        res.send(results);
        return;
      })
      .catch(error => {
        res.status(400);
        res.end();
      });
  });

  router.get(
    "/getApplications",
    passport.authenticate("oauth-bearer", { session: false }),
    function(req, res) {
      console.log("API call : getApplications");
      return storage
        .getApplications()
        .then(results => {
          res.send(results);
          return;
        })
        .catch(error => {
          res.status(400);
          res.end();
        });
    }
  );

  router.get("/getApplication", function(req, res) {
    console.log("API call : getApplication");
    let applicationId = req.query.id;
    return storage
      .getApplication(applicationId)
      .then(results => {
        res.send(results);
        return;
      })
      .catch(error => {
        res.status(400);
        res.end();
      });
  });

  router.get(
    "/getExaminationByStudent",
    passport.authenticate("oauth-bearer", { session: false }),
    function(req, res) {
      let id = req.query.id;
      storage
        .getExaminationByStudent(id)
        .then(results => {
          return res.send(results);
        })
        .catch(error => {
          res.status(400);
          res.end();
        });
    }
  );

  router.post("/verifyExaminer", function(req, res) {
    let examinationId = req.body.examinationId;
    let examinerId = req.body.examinerId;
    storage
      .verifyExaminer(examinationId, examinerId)
      .then(results => {
        return res.send(results);
      })
      .catch(error => {
        res.status(400);
        res.end();
      });
  });

  router.get("/getExaminationPortal", function(req, res) {
    let examinationId = req.query.examinationId;
    let examinerId = req.query.examinerId;
    storage
      .getExaminationPortal(examinationId, examinerId)
      .then(results => {
        return res.send(results);
      })
      .catch(error => {
        res.status(400);
        res.end();
      });
  });

  router.post(
    "/createExamination",
    passport.authenticate("oauth-bearer", { session: false }),
    function(req, res) {
      let examination = req.body;
      return storage
        .createExamination(examination)
        .then(results => {
          return res.end();
        })
        .catch(error => {
          res.status(400);
          res.end();
        });
    }
  );

  return router;
};
