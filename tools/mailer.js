"use strict";
const nodemailer = require("nodemailer");
const handlebars = require("handlebars");
const fs = require("fs");
const util = require("util");

fs.readFileAsync = util.promisify(fs.readFile);

// Generate test SMTP service account from ethereal.email
// Only needed if you don't have a real mail account for testing

// create reusable transporter object using the default SMTP transport
class Mailer {
  constructor() {
    this.transporter = nodemailer.createTransport({
      service: "gmail",
      host: "smtp.gmail.com",
      auth: {
        user: "maties.e.and.e@gmail.com", // generated ethereal user
        pass: "Tester123" // generated ethereal password
      },
      tls: {
        rejectUnauthorized: false
      }
    });
  }

  // setup email data with unicode symbols

  // send mail with defined transport object
  sendMail(application) {
    return new Promise((resolve, reject) => {
      fs.readFileAsync("./assets/apply.html", "utf8")
        .then(file => {
          var templateScript = handlebars.compile(file);
          var context = {
            full_name_surname: application.full_name_surname,
            uuid: application.id
          };
          var html = templateScript(context);

          this.mailOptions = {
            from: '"Postgraduate System" <no-reply@sun.ac.za>', // sender address
            to: application.email, // list of receivers
            subject: "Postgraduate Application", // Subject line
            html: html // html body
          };

          return this.transporter.sendMail(this.mailOptions, (error, info) => {
            if (error) {
              return reject(error);
            }
            return resolve(info.messageId);
          });
        })
        .catch(error => {
          return reject(error);
        });
    });
  }

  sendMailExaminer(examinationId, examinerId, email) {
    return new Promise((resolve, reject) => {
      fs.readFileAsync("./assets/examiner.html", "utf8")
        .then(file => {
          var templateScript = handlebars.compile(file);
          var context = {
            examinationId: examinationId,
            examinerId: examinerId
          };
          var html = templateScript(context);

          this.mailOptions = {
            from: '"Postgraduate System" <no-reply@sun.ac.za>', // sender address
            to: email, // list of receivers
            subject: "Postgraduate Examination", // Subject line
            html: html // html body
          };

          return this.transporter.sendMail(this.mailOptions, (error, info) => {
            if (error) {
              return reject(error);
            }
            return resolve(info.messageId);
          });
        })
        .catch(error => {
          return reject(error);
        });
    });
  }
}

module.exports = Mailer;
