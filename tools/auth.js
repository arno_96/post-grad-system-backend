const secrets = require("../secrets.json");
const credentials = {
  client: {
    id: secrets.APP_ID,
    secret: secrets.APP_PASSWORD
  },
  auth: {
    tokenHost: "https://login.microsoftonline.com",
    authorizePath: "common/oauth2/v2.0/authorize",
    tokenPath: "common/oauth2/v2.0/token"
  }
};
const oauth2 = require("simple-oauth2").create(credentials);
const jwt = require("./jwt");

function getAuthUrl() {
  return new Promise((resolve, reject) => {
    const authorizationUri = oauth2.authorizationCode.authorizeURL({
      redirect_uri: secrets.REDIRECT_URI,
      scope: secrets.APP_SCOPES
    });
    console.log(`Generated auth url: ${authorizationUri}`);
    return resolve({ url: authorizationUri });
  });
}
getTokenFromCode = auth_code => {
  return new Promise((resolve, reject) => {
    oauth2.authorizationCode
      .getToken({
        code: auth_code,
        redirect_uri: secrets.REDIRECT_URI,
        scope: secrets.APP_SCOPES
      })
      .then(result => {
        const token = oauth2.accessToken.create(result);
        //console.log("Token created: ", token.token);
        return resolve(token.token);
      })
      .catch(error => {
        console.log(error.data.payload);
        return reject(error.data.payload);
      });
  });
};

exports.getAuthUrl = getAuthUrl;
exports.getTokenFromCode = getTokenFromCode;
