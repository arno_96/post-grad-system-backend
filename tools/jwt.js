const jwt = require("jsonwebtoken");

function verifyToken(token) {
  return new Promise((resolve, reject) => {
    jwt.verify(token, "zwpGDWJD709;(@bnqlPE89)", function(err, decoded) {
      if (err) {
        console.log("ERR: " + err);
        reject(err);
      } else {
        console.log("decoded: " + decoded);
        resolve(decoded);
      }
    });
  });
}

function decodeToken(token) {
  return new Promise((resolve, reject) => {
    resolve(jwt.decode(token, { complete: true }));
  });
}

exports.decodeToken = decodeToken;
