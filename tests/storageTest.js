const Storage = require("../storage/firestore");
let storage = new Storage();

let student = {
  title: "Mr",
  initials: "FJ",
  name: "Frikkie",
  surname: "Badenhorst",
  display_name: "Frikkie Badenhorst",
  email: "arnovandermerwe39@gmail.com",
  su_number: "18971261",
  programme: "MEng",
  discipline: "Electrical & Electronic",
  topic: "Smart Solar Grid",
  primary_supervisor: "HA Engelbrecht",
  full_time: true,
  cell: "0736005798"
};

let submission = {
  url:
    "https://firebasestorage.googleapis.com/v0/b/e-and-e-postgraduate-system.appspot.com/o/432d4119-bdab-4b13-944e-cea70586e4bf?alt=media&token=aff24499-d4fd-4756-b8bc-6cdbf8c2a7a4",
  timestamp: "Sep 26, 2018, 11:46:00 AM"
};

//storage.createStudent(student);
//storage.getExamination("aGhTgif0wk5o1gGf5KDH");
//storage.uploadSubmission("5GhTgif0wk5o1gGf5KDH", submission);
storage.getExaminationPortal(
  "1b12d971-09c0-4bbf-9d56-ef157d3e421e",
  "87b7a128-4d1a-4c6b-8d34-cc664db15ccc"
);
