const functions = require("firebase-functions");

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser");
const logger = require("morgan");
const cors = require("cors");
const dotenv = require("dotenv").config();
const passport = require("passport");
const BearerStrategy = require("passport-azure-ad").BearerStrategy;
const secrets = require("./secrets.json");

var options = {
  identityMetadata:
    "https://login.microsoftonline.com/common/v2.0/.well-known/openid-configuration/",
  clientID: secrets.APP_ID,
  validateIssuer: false,
  loggingLevel: "warn",
  passReqToCallback: false
};

var bearerStrategy = new BearerStrategy(options, (token, done) => {
  // Send user info using the second argument
  done(null, {}, token);
});

var app = express();

app.use(cors({ origin: true }));
app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(passport.initialize());
passport.use(bearerStrategy);

var indexRouter = require("./routes/index");
var apiRouter = require("./routes/api")(passport);

app.use("/", indexRouter);
app.use("/api", apiRouter);

app.get(
  "/hello",
  passport.authenticate("oauth-bearer", { session: false }),
  (req, res) => {
    var claims = req.authInfo;
    console.log("User info: ", req.user);
    console.log("Validated claims: ", claims);

    res.status(200).json({ name: claims["name"] });
  }
);

exports.app = functions.https.onRequest(app);
